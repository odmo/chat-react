import React, { useState, useEffect, useMemo }from 'react'
import io from 'socket.io-client'

function App() {

  const [message, setMessage ] = useState('')
  const [messages, setMessages] = useState([])

  useEffect(() => {
    socket.on('message', reciveMessage)
    // apagar el evento cuando se desmonta el componente
    return () => {socket.off('message', reciveMessage)}
  },[])

  const socket = useMemo(() => (io("http://localhost:3000")),[])

  function handleSubmit(e) {
    e.preventDefault()
    setMessages([...messages, {
      body: message,
      from: 'Me',
    }])
    socket.emit('message', message)
  }

  function reciveMessage(message) {
    setMessages((state) => [...state, message])
  }

  return (
    <div className='h-screen bg-zinc-800 text-white flex items-center justify-center'>
      <form onSubmit={handleSubmit} className='bg-zinc-900 p-10'>
        <h1 className='text-2xl font-bold my-2'>Chat React</h1>
        <input 
          type="text"  
          className='border-2 bg-zinc-500 p-2 w-full text-black'
          placeholder='Write your message...'
          onChange={e => setMessage(e.target.value)}  
        />
        <ul>
        {messages.map((message, index) => (
          <li 
            key={index} 
            className={
              `my-2 p-2 table text-sm rounded-md ${ message.from === 'Me' ? 'bg-sky-700 ml-auto': 'bg-black' }`
            }>
            <span className='text-xs text-slate-300 font-bold block'>{message.from}</span>
            <span className='text-md'>{message.body}</span>
          </li>
        ))}
      </ul>
      </form>
      
    </div>
  )
}

export default App
