import express from 'express'
import http from 'http'
import { Server as SocketServer } from 'socket.io'

const app = express()
const server = http.createServer(app)
const io = new SocketServer(server, {
    cors: {
        origin: 'http://localhost:5173'
    }
})

io.on('connection', (socket) => {
    console.log('New connection', socket.id)
    // este evento lo escucha el backend del cliente
    socket.on('message', (data) => {
        // este evento lo escucha el cliente del backend
        socket.broadcast.emit('message', {
            body: data, 
            from: socket.id,
        })
    })
})

server.listen(3000, () => console.log('Server running on port 3000'))